#include "stdafx.h"
#include "Organizer.h"


Organizer::Organizer()
{
	SetNumbers(std::list<Number>());
}

Organizer::Organizer(std::list<Number> lNumbers)
{
	SetNumbers(lNumbers);
}

std::list<Number> Organizer::GetNumbers()
{
	return m_lNumbers;
}

void Organizer::SetNumbers(std::list<Number> lNumbers)
{
	m_lNumbers = lNumbers;
}

Number Organizer::GetNumberByName(std::string sName)
{
	for (auto iterator = m_lNumbers.begin(); iterator != m_lNumbers.end(); iterator++) {
		if ((*iterator).GetName() == sName) {
			return *iterator;
		}
	}
}

std::list<Number> Organizer::GetNumbersByPartOfName(std::string sName)
{
	std::list<Number> lNumbers = std::list<Number>();
	for (auto iterator = m_lNumbers.begin(); iterator != m_lNumbers.end(); iterator++) {
		if ((*iterator).GetName().find(sName.c_str())) {
			lNumbers.push_back(*iterator);
		}
	}
	return lNumbers;
}

void Organizer::DeleteNumberByName(std::string sName)
{
	for (auto iterator = m_lNumbers.begin(); iterator != m_lNumbers.end(); iterator++) {
		if ((*iterator).GetName() == sName) {
			m_lNumbers.erase(iterator);
		}
	}
}

void Organizer::WriteDataInFile(std::string sFileName)
{
	std::ofstream outfile(sFileName);
	for (auto iterator = m_lNumbers.begin(); iterator != m_lNumbers.end(); iterator++) {
		outfile << iterator->GetName().c_str() << " " << iterator->GetNumber() << " " << iterator->GetEmail().c_str() << std::endl;
	}
	outfile.close();
}

void Organizer::ReadDataFromFile(std::string sFileName)
{
	std::ifstream file(sFileName);
	char* sName;
	char* sEmail;
	int iNumber;
	while (file >> sName >> iNumber >> sEmail)
	{
		Number number(sName, iNumber, sEmail);
	}
	file.close();
}
