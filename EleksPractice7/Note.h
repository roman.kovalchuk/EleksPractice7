#pragma once
#include <iostream>
class Note
{
private:
	std::string m_sText;
	std::string m_sHeader;
	std::string m_sDate;
public:
	Note();
	Note(std::string sText, std::string sHeader);
	std::string GetText();
	std::string GetHeader();
	std::string GetDate();
	void SetDate(std::string sDate);
	void SetText(std::string sText);
	void SetHeader(std::string sHeader);

};

