#pragma once
#include <list>
#include <fstream>
#include "Number.h"
class Organizer
{
private:
	std::list<Number> m_lNumbers;
public:
	Organizer();
	Organizer(std::list<Number> lNumbers);
	std::list<Number> GetNumbers();
	void SetNumbers(std::list<Number> lNumbers);
	Number GetNumberByName(std::string sName);
	std::list<Number> GetNumbersByPartOfName(std::string sName);
	void DeleteNumberByName(std::string sName);
	void WriteDataInFile(std::string sFileName);
	void ReadDataFromFile(std::string sFileName);
};

