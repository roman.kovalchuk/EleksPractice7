#pragma once
#include <iostream>
class Number
{
private:
	int m_iNumber;
	std::string m_sName;
	std::string m_sEmail;

public:
	Number();
	Number(std::string sName, int iNumber, std::string sEmail);
	std::string GetName();
	void SetName(std::string sName);
	std::string GetEmail();
	void SetEmail(std::string sEmail);
	int GetNumber();
	void SetNumber(int iNumber);
};

