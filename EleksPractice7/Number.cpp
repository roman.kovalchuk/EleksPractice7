#include "stdafx.h"
#include "Number.h"


Number::Number()
{
	SetName("");
	SetEmail("");
	SetNumber(0);
}

Number::Number(std::string sName, int iNumber, std::string sEmail)
{
	SetName(sName);
	SetEmail(sEmail);
	SetNumber(iNumber);
}

std::string Number::GetName()
{
	return m_sName;
}

void Number::SetName(std::string sName)
{
	m_sName = sName;
}

std::string Number::GetEmail()
{
	return m_sEmail;
}

void Number::SetEmail(std::string sEmail)
{
	m_sEmail = sEmail;
}

int Number::GetNumber()
{
	return m_iNumber;
}

void Number::SetNumber(int iNumber)
{
	m_iNumber = iNumber;
}
