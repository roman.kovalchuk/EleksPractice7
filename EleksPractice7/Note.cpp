#include "stdafx.h"
#include "Note.h"


Note::Note()
{
	SetDate("");
	SetHeader("");
	SetText("");
}

Note::Note(std::string sText, std::string sHeader)
{
	SetHeader(sHeader);
	SetText(sText);
}

std::string Note::GetText()
{
	return m_sText;
}

std::string Note::GetHeader()
{
	return m_sHeader;
}

std::string Note::GetDate()
{
	return m_sDate;
}

void Note::SetDate(std::string sDate)
{
	m_sDate = sDate;
}

void Note::SetText(std::string sText)
{
	m_sText = sText;
}

void Note::SetHeader(std::string sHeader)
{
	m_sHeader = sHeader;
}

